import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';

configure({ adapter: new Adapter() });


const setup = (additionalProps = {}) => {
  return shallow(<App {...additionalProps} />);
}
describe('App', () => {
  it('renders without crashing', () => {
    expect(setup).not.toThrow();
  });
})