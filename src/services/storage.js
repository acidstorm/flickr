export const getFavourites = () => {
  return JSON.parse(sessionStorage.getItem('favourites') || '{}')
}

export const saveFavourites = (favourites) => {
  sessionStorage.setItem('favourites', JSON.stringify(favourites));
}