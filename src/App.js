import React from 'react';
import logo from './logo.svg';
import './App.css';
import Gallery from './components/Gallery';

const App = () => {
  return (
    <div className="App">
      <Gallery />
    </div>
  );
}

export default App;
