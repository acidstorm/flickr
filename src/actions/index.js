import { createAction } from 'redux-actions';
export const actionTypes = {
  ADD_IMAGES: 'ADD_IMAGES',
  TOGGLE_FAVOURITE: 'TOGGLE_FAVOURITE',
};

export const toggleFavourite = createAction(actionTypes.TOGGLE_FAVOURITE);
export const addImages = createAction(actionTypes.ADD_IMAGES);
