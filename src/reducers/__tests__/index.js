jest.mock('../../services/storage');
import reducer from '../';

const initialState = {
  favourites: {},
  imagesById: {}
};

describe('Reducers', () => {
  describe('TOGGLE_FAVOURITE reducer', () => {
    it('updates state correctly', () => {
      const id = 'image_link';
      const payload = {
        type: 'TOGGLE_FAVOURITE',
        payload: {
          id: 'image_link'
        }
      };
      const expectedState = {
        favourites: {
          [id]: true
        },
        imagesById: {}
      };
      const newState = reducer(initialState, payload);
      expect(newState).toEqual(expectedState);
    });
  });

  describe('ADD_IMAGES reducer', () => {
    it('updates state correctly', () => {
      const id = 'image_link';
      const id2 = 'image_link_2';
      const payload = {
        type: 'ADD_IMAGES',
        payload: [
          {
            link: id,
          },
          {
            link: id2,
          }
        ]
      };
      const expectedState = {
        favourites: {},
        imagesById: {
          [id]: {
            link: id,
          },
          [id2]: {
            link: id2,
          }
        }
      };
      const newState = reducer(initialState, payload);
      expect(newState).toEqual(expectedState);
    });
  })
})