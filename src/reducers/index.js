import { actionTypes, addItems } from'../actions';
import { saveFavourites, getFavourites } from '../services/storage';
import { parseFavourites } from '../utils';

const initialState = {
  favourites: {},
  imagesById: {}
};

export default (state = initialState, action) => {
  const { payload , type } = action;
  switch(type) {
    case actionTypes.TOGGLE_FAVOURITE:
      let { favourites } = state;
      // Toggle image favourite status.
      // I could use delete here, but I don't for 2 reasons
      // 1. Preserving the semantics of the function. It's a toggle.
      // 2. deleting a key makes it harder for v8 to optimise.
      // Disadvantage: Memory usage.
      favourites[payload.id] = !favourites[payload.id]
      saveFavourites(favourites);
        return {
          ...state,
          favourites
        }
    break;
    case actionTypes.ADD_IMAGES:
      let imagesById = payload.reduce((accumulator, image) => {
        // Use image links as key since they're unlikely to change
        accumulator[image.link] = image;
        return accumulator;
      }, {});

      const storedFavourites = getFavourites();
      let parsedFavourites = parseFavourites(storedFavourites, imagesById);
      return {
        ...state,
        imagesById,
        favourites: parsedFavourites
      }
    break;
    default:
      return {
        ...state
      }
    break;
  }
};