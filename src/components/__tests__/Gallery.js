import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import { render, shallow, configure } from 'enzyme';
import { Gallery, GalleryImage, displayFavouritesText } from '../Gallery';

configure({ adapter: new Adapter() });


const setup = (additionalProps = {}) => {
  return render(<Gallery {...additionalProps} />);
}

describe('Gallery', () => {
  it('renders without crashing', () => {
    expect(setup).not.toThrow();
  });

  it('renders the right number of gallery images', () => {
    const images = [
      {
        link: 'Link 1',
        media: 'Media 1'
      },
      {
        link: 'Link 2',
        media: 'Media 2'
      },
      {
        link: 'Link 3',
        media: 'Media 3'
      },
    ];
    const wrapper = setup({
      images
    });

    //  Extra block for load more text
    expect(wrapper.find('.gallery_item').length).toEqual(images.length + 1);
  })
});

describe('GalleryImage', () => {
  const media = {
    m: 'Media'
  };
  const isFavourite =  false;
  const handleClick = jest.fn();
  const defaultProps = {
    image: {
      media,
    },
    isFavourite,
    handleClick
  };
  it('renders without errors', () => {
    expect(shallow.bind(null, <GalleryImage {...defaultProps} />)).not.toThrow();
  });

  it('renders non-favourited image', () => {
    let props = {
      ...defaultProps
    };
    const wrapper = shallow(<GalleryImage {...props} />);
    expect(wrapper.find('.gallery_item.favourite').length).toEqual(0);
    expect(wrapper.find('.gallery_item').length).toEqual(1);
  });

  it('renders favourited image', () => {
    let props = {
      ...defaultProps,
      isFavourite: true
    };
    const wrapper = shallow(<GalleryImage {...props} />);
    expect(wrapper.find('.gallery_item.favourite').length).toEqual(1);
  });

});

describe('displayFavouritesText', () => {
  it('returns the correct text', () => {
    expect(displayFavouritesText([])).toEqual('');
    expect(displayFavouritesText([{}])).toEqual('1 image added to favourites');
    expect(displayFavouritesText([{}, {}])).toEqual('2 images added to favourites');
  });
})