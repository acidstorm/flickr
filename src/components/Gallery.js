import { connect } from 'react-redux';
import React from 'react';
import { toggleFavourite } from '../actions'

export const GalleryImage = ({image: { media, link }, isFavourite, handleClick }) => {
  const style = {
    backgroundImage: `url(${media.m})`,
  };

  const favourite = isFavourite ? 'selected favourite' : '';
  return (
    <div key={link} className={`${favourite} gallery_item`} style={style} onClick={handleClick.bind(null, { id: link })}></div>
  );
}

// Returns text to display give a list of user favourites
export const displayFavouritesText = (favourites = []) => {
  const favouritesCount = favourites.length;
  switch(favouritesCount) {
    case 0:
     return '';
    break;
    case 1:
      return '1 image added to favourites';
    break;
    default:
      return `${favouritesCount} images added to favourites`;
    break;
  }
}

export const Gallery = ({ images = [], favourites = {}, handleClick, selectedText}) => {
  const gallery = images.map(image => GalleryImage({
    isFavourite: !!favourites[image.link],
    image,
    handleClick
  }));

  //  Filter out favourites so we can get a count
  const favouriteIds = Object.entries(favourites).filter(([id]) => favourites[id]);

  return (
      <div>
      <h3 className="gallery_header">Images tagged 'London' on Flickr <span>{displayFavouritesText(favouriteIds)}</span></h3>
      <div className="gallery_wrapper">
        <div className="gallery_list">
        {gallery}
        { images.length ?
          <a key={"fetch_next"} className="gallery_item gallery_item--next_link">Load more...</a> :
          null
        }
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = ({ favourites, imagesById }) => {
  return {
    favourites,
    images: Object.entries(imagesById).map(([_, image]) => {
      return image;
    })
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleClick: (link) => dispatch(toggleFavourite(link))
  };
}

Gallery.defaultProps = {
  handleClick: _ => _
}
export default connect(mapStateToProps, mapDispatchToProps)(Gallery);