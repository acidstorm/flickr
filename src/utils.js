//  Parse favourites. Prunes favourites as well to minimise memory usage.
export const parseFavourites = (favourites = {}, database = {}) => {
  return Object.keys(favourites)
  .filter(id => favourites[id] && database[id])
  .reduce((accumulator, id) => {
    accumulator[id] = true;
    return accumulator;
  }, {});
}