import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { addImages, toggleFavourite } from './actions';
import reducer from './reducers';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Gallery from './components/Gallery';


const store = createStore(reducer);

(function () {
  window.cb = (data) => {
    store.dispatch(addImages(data.items));
  }
  var tags = 'london';
  var script = document.createElement('script');
  script.src = `http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=cb&tags=${tags}`;
  document.head.appendChild(script);
})();


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'));
registerServiceWorker();
